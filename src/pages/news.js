import React from 'react';

import Layout from '../components/Layout';
import Header from '../components/Header';
import { Link } from 'gatsby';
import news1 from '../assets/images/peta.png';
import news2 from '../assets/images/life_honor_jinchao_xu.jpg';
import Footer from '../components/Footer';
import List from '../components/List';
import News from '../components/News';
const IndexPage = () => (
  <Layout>
    <Header />

    <section id="about" className="about-section text-center">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 mx-auto">
            <h2 className="text-white mb-4"> Get to know our most recent updates!</h2>
            <ol className='text-white text-left'>
            <List page='news' time='2019-11-01'>Kick off meeting held in MSC N308A. </List>
            <List page='news' time='2019-11-26'>AAAS fellows, professor Jinchao Xu was recognized with lifetime honor! </List>
            </ol>

          </div>
        </div>
      </div>
    </section>

    <section id="news" className="news-section bg-light">
      <News time='2019-11-01' title='Kick off meeting held in MSC N308A.' img={news1} > </News>
    <News time='2019-11-26' title='AAAS fellows, professor Jinchao Xu was recognized with lifetime honor!' img={news2} > 
        On 26 November 2019, AAAS announced Leading Scientists Elected as 2019 Fellows. Professor Jinchao Xu, the CCMA director, was among the seven mathematicians in the world named as 2019 AAAS Fellow. And he also was invited to give a presentation at the AAAS mathematics sections (Section A) business meeting.
        During the meeting, Jinchao was invited to give a presentation at the AAAS mathematics sections (Section A) business meething on the research that was cited as part of his nomination.
        </News>
    </section>


    <Footer />

  </Layout>
);

export default IndexPage;
