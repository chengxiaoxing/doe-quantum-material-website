import React from 'react';

import Layout from '../components/Layout';
import Scroll from '../components/Scroll';

import config from '../../config';
import Footer from '../components/Footer';
import SocialLinks from '../components/SocialLinks';
import Subscribe from '../components/Subscribe';
import Header from '../components/Header';

// import ipad from '../assets/images/ipad.png';
// import aboutImg from '../assets/images/pnr-light.png';
import aboutImg from '../assets/images/about.png';
import chenGroup from '../assets/images/chen-group.jpg';
import daboGroup from '../assets/images/dabo-group.jpg';
import gopalanGroup from '../assets/images/gopalan-group.png';
import xuGroup from '../assets/images/xu-group.png';
import herbertGroup from '../assets/images/herbert-group.png';
import demoImage1 from '../assets/images/demo-image-01.jpg';
import demoImage2 from '../assets/images/demo-image-02.jpg';
// import bgMaster from '../assets/images/bg-masthead.jpg';
import bgMaster from '../assets/images/final-wave-small.jpg';
import vo2 from '../assets/images/EthofT.png';
import news1 from '../assets/images/peta.png';
import ultrafast from '../assets/images/ultrafast.png';
import topological from '../assets/images/topological.jpg';

const IndexPage = () => (
  <Layout>
    <Header />

    <header className="masthead">
      <div className="container d-flex h-100 align-items-center">
        <div className="mx-auto text-left">
          <div><h1 className="d-inline-block text-center w3rem">M</h1> <h1 className="d-inline-block">esoscale</h1></div>
          <div><h1 className="d-inline-block text-center w3rem">O</h1> <h1 className="d-inline-block">pen-source</h1></div>
          <div><h1 className="d-inline-block text-center w3rem">S</h1> <h1 className="d-inline-block">imulation</h1></div>
          <div><h1 className="d-inline-block text-center w3rem">T</h1> <h1 className="d-inline-block">oolkit</h1></div>
          <div><h1 className="d-inline-block text-center w3rem">&nbsp;</h1> <h1 className="d-inline-block">for</h1></div>
          <div><h1 className="d-inline-block text-center w3rem">Q</h1> <h1 className="d-inline-block">uantum</h1></div>
          <div><h1 className="d-inline-block text-center w3rem">M</h1> <h1 className="d-inline-block">aterial</h1></div>


          {/* <h2 className="text-white-50 mx-auto mt-2 mb-5">
          </h2> */}
          {/* <Scroll type="id" element="about">
            <a href="#about" className="btn btn-primary">
              About
            </a>
          </Scroll> */}
        </div>
      </div>
    </header>

    <section id="about" className="about-section text-center">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 mx-auto">
            <h2 className="text-white mb-4">DOE Computational Materials Science Software Project: Mesoscale Science and Open Software for Quantum Materials
</h2>
            <p className="text-white text-justify">
            This is an integrated computational, software development and experimental validation program to advance the science of quantum materials at the mesoscale between the atomic scale and the macroscopic continuum. 
            The spatiotemporal modeling, understanding, and control at the mesoscale is critical to translating novel quantum phenomena into functional devices and systems.  
            In particular, we aim to incorporate the essential physics of electrons, including strong electron correlation, topologically nontrivial spin, charge, orbital and lattice textures and their dynamical phenomena on the ultrafast time scales, 
            into the computational phase-field method that can predict emergent mesoscale quantum orders and pattern formations from the femtosecond to equilibrium time scales. 
            </p>
            <p className="text-white text-justify">
            The program brings together an interdisciplinary team of experts in mesoscale phase-field modeling (Chen), advanced numerical algorithms and high-performance computing (Xu), materials thermodynamic database development based on electronic structure calculations (Dabo), 
            and experimental validation using finite-temperature measurements involving nonlinear optical microscopy, scanning probe microscopy and diffraction imaging (Gopalan), as well as cutting-edge materials synthesis using molecular beam epitaxy (Engel-Herbert).  
            </p>
            <p className="text-white text-justify">
            The specific objectives of the proposed program are to: (1) explore and understand the thermodynamic stability of mesoscale structures and their responses to external thermal, mechanical, electric, and magnetic stimuli; 
            (2) to develop mesoscale computational models, innovative numerical algorithms for exascale computations, and implement them into a corresponding open-source software for understanding, discovering and manipulating emergent mesoscale architectures and phenomena in quantum materials, 
            and (3) experimentally validate and then refine the theory and computational tools using materials synthesis with atomic scale control and cutting-edge characterization of mesoscale structural and functional properties of quantum materials. 
            One of the primary outcomes of the proposed project is an experimentally validated open source software package, open Q-&micro;-PRO, parallelized to enable peta- and exascale computing for understanding and predicting quantum materials and their mesoscale responses to external thermal, chemical, electrical, magnetic, and mechanical stimuli towards designing device architectures for harnessing these functionalities.
            </p>
            {/* <p className="text-white-50 text-justify">Our team is made up of 5 professors. 
            Dr. Long-Qing Chen (left first), PI, Donald W. Hamer Professor of Materials Science and Engineering, Professor of Engineering Science and Mechanics, and Mathematics. 
            Dr. Ismaila Dabo (middle), Associate Professor of Materials Science and Engineering. 
            Dr. Roman Engel-Herbert (right first), Associate Professor of Materials Science and Engineering and Physics.
            Dr. Venkatraman Gopalan (right second), Professor of Materials Science and Engineering and Physics.
            Dr. Jinchao Xu (left second), Verne M. Willaman Professor of Mathematics, Affiliate Professsor of Information Sciences and Technology, Director of CCMA.
            </p> */}
            <p className="text-white text-justify">This program is supported as part of the Computational Materials Sciences Program funded by the US Department of Energy, Office of Science, Basic Energy Sciences, under Award Number DE-SC0020145.
            </p>
          </div>
        </div>
        <img src={aboutImg} className="img-fluid" alt="" />
      </div>
    </section>

    <section id="teams" className="teams-section bg-light">
      <div className="container">
        <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
          <div className="col-lg-6">
            <img className="img-fluid" src={chenGroup} alt="" />
          </div>
          <div className="col-lg-6">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="team-text w-100 my-auto text-center text-lg-left">
                  <h4 className="text-white">Long-Qing Chen's group</h4>
                  <p className="mb-0 text-white-50">
                    <a href='https://www.mri.psu.edu/chen-research-group'>Phase-field simulation</a>
                  </p>
                  <hr className="d-none d-lg-block mb-0 ml-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters">
          <div className="col-lg-6">
            <img className="img-fluid" src={gopalanGroup} alt="" />
          </div>
          <div className="col-lg-6 order-lg-first">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="team-text w-100 my-auto text-center text-lg-right">
                  <h4 className="text-white">Venkatraman Gopalan's group</h4>
                  <p className="mb-0 text-white-50">
                  <a href='https://www.mri.psu.edu/gopalan-group/gopalan-group'>Pump-probe experiment, COBRA</a>
                  </p>
                  <hr className="d-none d-lg-block mb-0 mr-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
          <div className="col-lg-6">
            <img className="img-fluid" src={herbertGroup} alt="" />
          </div>
          <div className="col-lg-6">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="team-text w-100 my-auto text-center text-lg-left">
                  <h4 className="text-white">Roman Engel-Herbert's group</h4>
                  <p className="mb-0 text-white-50">
                    <a href='https://www.matse.psu.edu/directory/roman-engel-herbert'>Hybrid MBE thin film growth</a>
                  </p>
                  <hr className="d-none d-lg-block mb-0 ml-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters">
          <div className="col-lg-6">
            <img className="img-fluid" src={daboGroup} alt="" />
          </div>
          <div className="col-lg-6 order-lg-first">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="team-text w-100 my-auto text-center text-lg-right">
                  <h4 className="text-white">Ismaila Dabo's group</h4>
                  <p className="mb-0 text-white-50">
                    <a href='https://dabo.matse.psu.edu/'>DFT, DFT+U calculation</a>
                  </p>
                  <hr className="d-none d-lg-block mb-0 mr-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
          <div className="col-lg-6">
            <img className="img-fluid" src={xuGroup} alt="" />
          </div>
          <div className="col-lg-6">
            <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                <div className="team-text w-100 my-auto text-center text-lg-left">
                  <h4 className="text-white">Jinchao Xu's group</h4>
                  <p className="mb-0 text-white-50">
                    <a href='http://www.personal.psu.edu/jxx1/' >Algebraic multigrid methods</a>
                  </p>
                  <hr className="d-none d-lg-block mb-0 ml-0" />
                </div>
              </div>
            </div>
          </div>
        </div>

        </div>
    </section>

    <section id="projects" className="projects-section bg-light">
      <div className="container">
        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
        <div className="col-xl-4 col-lg-5">
            <div className="featured-text-right text-center text-lg-right">
              <h4>Strongly correlated systems</h4>
              <p className="text-black-50 mb-0">
              Electron correlation effects are the manifestation of a sizeable electron-electron 
              interaction strength affecting the electronic states in materials.  
              In particular, we will focus on two such critical phenomena: 
              Metal-Insulator transitions, and Superconductivity.
              </p>
            </div>
          </div>
          <div className="col-xl-8 col-lg-7">
            <img className="img-fluid mb-3 mb-lg-0" src={vo2} alt="" />
          </div>

        </div>

        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
        <div className="col-xl-4 col-lg-5">
            <div className="featured-text-right text-center text-lg-right">
              <h4>Topological Textures</h4>
              <p className="text-black-50 mb-0">
              Recent theoretical and experimental discoveries of magnetic and polar topological textures shows dramatical
              difference from the classical domain patterns, since the topological structures 
              are insensitive to small perturbations, so called "topological protection".
              </p>
            </div>
          </div>
          <div className="col-xl-8 col-lg-7">
            <img className="img-fluid mb-3 mb-lg-0" src={topological} alt="" />
          </div>

        </div>

        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
        <div className="col-xl-4 col-lg-5">
            <div className="featured-text-right text-center text-lg-right">
              <h4>Ultrafast dynamics</h4>
              <p className="text-black-50 mb-0">
              Applying the dynamical phase-field model to the experimentally observed laser-activated ultrafast domain and lattice dynamics in ferroelectric
              materials.
              </p>
            </div>
          </div>
          <div className="col-xl-8 col-lg-7">
            <img className="img-fluid mb-3 mb-lg-0" src={ultrafast} alt="" />
          </div>

        </div>
      </div>
    </section>

    <section id="publications" className="publications-section bg-light">
      <div className="container">
        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
          <div className="col-xl-12 col-lg-12">
            {/* <ul className=" text-center text-lg-left">
              <li className='featured-text'>
              <a href='https://iopscience.iop.org/article/10.1209/0295-5075/120/46003/meta'>
              <h4>Ginzburg-Landau theory of metal-insulator transition in VO2: The electronic degrees of freedom</h4>
              <p className="text-black-50 mb-0">
              Shi, Yin, Fei Xue, and Long-Qing Chen. EPL (Europhysics Letters) 120, no. 4 (2018): 46003.
              </p>
              </a>
              </li>

              <li className='featured-text'>
              <a href='https://iopscience.iop.org/article/10.1209/0295-5075/120/46003/meta'>
              <h4>Phase-field model of insulator-to-metal transition in VO 2 under an electric field.</h4>
              <p className="text-black-50 mb-0">
              Shi, Yin, and Long-Qing Chen. Physical Review Materials 2, no. 5 (2018): 053803.
              </p>
              </a>
              </li>

            </ul> */}
          </div>
        </div>

      </div>
    </section>

    <Subscribe />

    <SocialLinks />
    <Footer />
  </Layout>
);

export default IndexPage;
