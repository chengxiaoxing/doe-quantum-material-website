import React,{Component} from 'react';
import { Link } from 'gatsby';
import Scroll from './Scroll';

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
          openMenu: false,
          visibilityClass: '',
        };
      }
      toggleMenu = value => {
        this.setState({ openMenu: value });
      };
      handleScroll = () => {
        const { visibilityClass } = this.state;
        if (window.pageYOffset > 100) {
          if (visibilityClass !== 'navbar-shrink') {
            this.setState({ visibilityClass: 'navbar-shrink' });
          }
        } else {
          if (visibilityClass === 'navbar-shrink') {
            this.setState({ visibilityClass: '' });
          }
        }
      };
    
      componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
      }
      componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
      }
    render() {
        const { openMenu, visibilityClass } = this.state;
        return (
            <li>
                <span className='text-white col-lg-2 mx-auto'>{this.props.time}</span>
                <span className='text-white col-lg-10 mx-auto'>
                    <Scroll
                  onClick={_ => this.toggleMenu(!openMenu)}
                  type="id"
                  element={`${this.props.time}`}
                  offset={-120}
                >
                  <a href={`${this.props.page}/#${this.props.time}`}>
                  {this.props.children}
                  </a>
                </Scroll>
                </span>
            </li>
        );
    }
}
