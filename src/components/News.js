import React,{Component} from 'react';
import { Link } from 'gatsby';

export default class News extends Component {
    constructor(props) {
        super(props);
      }
    render() {
        return (
            <div className='container'>
            <div className="row align-items-center justify-content-md-center no-gutters mb-4 mb-lg-5" id={`${this.props.time}`}>
                <div className="col-xl-4 col-lg-5">
                    <img className="img-fluid mb-3 mb-lg-0" src={this.props.img} alt="" />
                </div>
                <div className="col-xl-7 col-lg-6">
                    <div className="featured-text text-center text-lg-left">
                    <h4>{this.props.time}</h4>
                    <p className="text-black-50 mb-0">
                    {this.props.title} 
                    </p>
                    </div>
                    <Link to='news/#page-top' className='float-right'>Back to top</Link>
                </div>
            <div className="row justify-content-md-center mt-3">
                <div className="col-lg-12 text-justify">{this.props.children}
                </div>
            </div>
        </div>
        </div>
        );
    }
}
