import React from 'react';

export default function Subscribe() {
  return (
    <section id="signup" className="signup-section">
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-lg-8 mx-auto text-center">
            <i className="far fa-paper-plane fa-2x mb-2 text-white"></i>
            <h2 className="text-white mb-5">Tell us about your ideas</h2>

            <form name="contact" method="POST" data-netlify='true'>
              <div class="form-inline d-flex row">
              <input
                type="email"
                className="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0"
                id="inputEmail"
                placeholder="Enter email address..."
              />
              <button type="submit" className="btn btn-primary mx-auto">
                Contact us
              </button>
              </div>
              <div class="form-inline d-flex row">
              <textarea
                type="text"
                className="form-control flex-fill mb-3 mb-sm-0 mt-2"
                id="inputText"
                placeholder="Your thoughts"
                rows="5"
              />
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
